package org.godotengine.godot;

import android.app.Activity;
import android.util.Log;

import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.InPlay.CBInPlay;
import com.chartboost.sdk.Libraries.CBLogging.Level;
import com.chartboost.sdk.Model.CBError.CBClickError;
import com.chartboost.sdk.Model.CBError.CBImpressionError;

/**
 * Created by alexey on 26.03.15.
 */

public class GodotChartboost extends Godot.SingletonBase {
    private static final String TAG = "Chartboost";

    private Activity activity = null;
    private boolean active = false;

    public GodotChartboost(Activity activity) {

        registerClass("Chartboost", new String[]
                {
                        "init",
                        "showInterstitial",
                        "showMoreApps",
                        "cacheInterstitial",
                        "cacheMoreApps",
                        "showRewardedVideo",
                        "cacheRewardedVideo"
                });

        this.activity = activity;
    }

    static public Godot.SingletonBase initialize(Activity p_activity)
    {
        return new GodotChartboost(p_activity);
    }

    public void init() {
        if (active)
            return;

        activity.runOnUiThread(new Runnable() {
            public void run() {
		String appId = GodotLib.getGlobal("chartboost/id");
		String appSignature = GodotLib.getGlobal("chartboost/signature");
                
		Chartboost.startWithAppId(activity, appId, appSignature);
                Chartboost.setLoggingLevel(Level.ALL);
                Chartboost.setDelegate(delegate);
                Chartboost.onCreate(activity);

		active = true;
		onStart();
            }
        });

       
    }

    protected void onStart() {
        if(active)
            Chartboost.onStart(activity);
    }

    protected void onStop() {
        if(active)
            Chartboost.onStop(activity);
    }

    //@Override
    protected void onMainPause() {
        if(active) {
            Chartboost.onPause(activity);
        }
    }

    //@Override
    protected void onMainResume() {
        if(active) {
            Chartboost.onResume(activity);
        }
    }

    public boolean onBackPressed() {
        if(active) {
            return Chartboost.onBackPressed();
        }

        return false;
    }

    //@Override
    protected void onMainDestroy() {
        if(active) {

            onStop();

            Chartboost.onDestroy(activity);
            active = false;
        }
    }

    public void showInterstitial() {
        if(!active)
            return;

        String toastStr = "Loading Interstitial";

        if (Chartboost.hasInterstitial(CBLocation.LOCATION_LEADERBOARD))
            toastStr = "Loading Interstitial From Cache";

        Log.i(TAG, toastStr);
        Chartboost.showInterstitial(CBLocation.LOCATION_LEADERBOARD);
    }

    public void showMoreApps() {
        if(!active)
            return;

        String toastStr = "Loading More Apps";

        if (Chartboost.hasMoreApps(CBLocation.LOCATION_GAME_SCREEN))
            toastStr = "Loading More Apps From Cache";

        Log.i(TAG, toastStr);
        Chartboost.showMoreApps(CBLocation.LOCATION_GAME_SCREEN);
    }

    public void cacheInterstitial() {
        if(!active)
            return;

        Log.i(TAG, "Preloading Interstitial Ad");
        Chartboost.cacheInterstitial(CBLocation.LOCATION_LEADERBOARD);
    }

    public void cacheMoreApps() {
        if(!active)
            return;

        Log.i(TAG, "Preloading More apps Ad");
        Chartboost.cacheMoreApps(CBLocation.LOCATION_GAME_SCREEN);
    }

    public void showRewardedVideo() {
        if(!active)
            return;

        String toastStr = "Loading Rewarded Interstitial";

        if (Chartboost.hasRewardedVideo(CBLocation.LOCATION_ACHIEVEMENTS))
            toastStr = "Loading Rewarded Interstitial From Cache";

        Log.i(TAG, toastStr);
        Chartboost.showRewardedVideo(CBLocation.LOCATION_ACHIEVEMENTS);
    }

    public void cacheRewardedVideo() {
        if(!active)
            return;

        Log.i(TAG, "Preloading Rewarded Interstitial Ad");
        Chartboost.cacheRewardedVideo(CBLocation.LOCATION_ACHIEVEMENTS);
    }

    private ChartboostDelegate delegate = new ChartboostDelegate() {

        @Override
        public boolean shouldRequestInterstitial(String location) {
            Log.i(TAG, "SHOULD REQUEST INTERSTITIAL '" + (location != null ? location : "null"));
            return true;
        }

        @Override
        public boolean shouldDisplayInterstitial(String location) {
            Log.i(TAG, "SHOULD DISPLAY INTERSTITIAL '" + (location != null ? location : "null"));
            return true;
        }

        @Override
        public void didCacheInterstitial(String location) {
            Log.i(TAG, "DID CACHE INTERSTITIAL '" + (location != null ? location : "null"));
        }

        @Override
        public void didFailToLoadInterstitial(String location, CBImpressionError error) {
            Log.i(TAG, "DID FAIL TO LOAD INTERSTITIAL '" + (location != null ? location : "null") + " Error: " + error.name());
            //Toast.makeText(getApplicationContext(), "INTERSTITIAL '" + location + "' REQUEST FAILED - " + error.name(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void didDismissInterstitial(String location) {
            Log.i(TAG, "DID DISMISS INTERSTITIAL: " + (location != null ? location : "null"));
        }

        @Override
        public void didCloseInterstitial(String location) {
            Log.i(TAG, "DID CLOSE INTERSTITIAL: " + (location != null ? location : "null"));
        }

        @Override
        public void didClickInterstitial(String location) {
            Log.i(TAG, "DID CLICK INTERSTITIAL: " + (location != null ? location : "null"));
        }

        @Override
        public void didDisplayInterstitial(String location) {
            Log.i(TAG, "DID DISPLAY INTERSTITIAL: " + (location != null ? location : "null"));
        }

        @Override
        public boolean shouldRequestMoreApps(String location) {
            Log.i(TAG, "SHOULD REQUEST MORE APPS: " + (location != null ? location : "null"));
            return true;
        }

        @Override
        public boolean shouldDisplayMoreApps(String location) {
            Log.i(TAG, "SHOULD DISPLAY MORE APPS: " + (location != null ? location : "null"));
            return true;
        }

        @Override
        public void didFailToLoadMoreApps(String location, CBImpressionError error) {
            Log.i(TAG, "DID FAIL TO LOAD MOREAPPS " + (location != null ? location : "null") + " Error: " + error.name());
            //Toast.makeText(getApplicationContext(), "MORE APPS REQUEST FAILED - " + error.name(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void didCacheMoreApps(String location) {
            Log.i(TAG, "DID CACHE MORE APPS: " + (location != null ? location : "null"));
        }

        @Override
        public void didDismissMoreApps(String location) {
            Log.i(TAG, "DID DISMISS MORE APPS " + (location != null ? location : "null"));
        }

        @Override
        public void didCloseMoreApps(String location) {
            Log.i(TAG, "DID CLOSE MORE APPS: " + (location != null ? location : "null"));
        }

        @Override
        public void didClickMoreApps(String location) {
            Log.i(TAG, "DID CLICK MORE APPS: " + (location != null ? location : "null"));
        }

        @Override
        public void didDisplayMoreApps(String location) {
            Log.i(TAG, "DID DISPLAY MORE APPS: " + (location != null ? location : "null"));
        }

        @Override
        public void didFailToRecordClick(String uri, CBClickError error) {
            Log.i(TAG, "DID FAILED TO RECORD CLICK " + (uri != null ? uri : "null") + ", error: " + error.name());
            //Toast.makeText(getApplicationContext(), "FAILED TO RECORD CLICK " + (uri != null ? uri : "null") + ", error: " + error.name(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public boolean shouldDisplayRewardedVideo(String location) {
            Log.i(TAG, String.format("SHOULD DISPLAY REWARDED VIDEO: '%s'", (location != null ? location : "null")));
            return true;
        }

        @Override
        public void didCacheRewardedVideo(String location) {
            Log.i(TAG, String.format("DID CACHE REWARDED VIDEO: '%s'", (location != null ? location : "null")));
        }

        @Override
        public void didFailToLoadRewardedVideo(String location,
                                               CBImpressionError error) {
            Log.i(TAG, String.format("DID FAIL TO LOAD REWARDED VIDEO: '%s', Error:  %s", (location != null ? location : "null"), error.name()));
            //Toast.makeText(getApplicationContext(), String.format("DID FAIL TO LOAD REWARDED VIDEO '%s' because %s", location, error.name()), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void didDismissRewardedVideo(String location) {
            Log.i(TAG, String.format("DID DISMISS REWARDED VIDEO '%s'", (location != null ? location : "null")));
        }

        @Override
        public void didCloseRewardedVideo(String location) {
            Log.i(TAG, String.format("DID CLOSE REWARDED VIDEO '%s'", (location != null ? location : "null")));
        }

        @Override
        public void didClickRewardedVideo(String location) {
            Log.i(TAG, String.format("DID CLICK REWARDED VIDEO '%s'", (location != null ? location : "null")));
        }

        @Override
        public void didCompleteRewardedVideo(String location, int reward) {
            Log.i(TAG, String.format("DID COMPLETE REWARDED VIDEO '%s' FOR REWARD %d", (location != null ? location : "null"), reward));
        }

        @Override
        public void didDisplayRewardedVideo(String location) {
            Log.i(TAG, String.format("DID DISPLAY REWARDED VIDEO '%s' FOR REWARD", location));
        }

        @Override
        public void willDisplayVideo(String location) {
            Log.i(TAG, String.format("WILL DISPLAY VIDEO '%s", location));
        }
    };
}
