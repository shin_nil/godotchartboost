def can_build(plat):
	return plat=="android"

def configure(env):
	if (env['platform'] == 'android'):
		env.android_add_dependency("compile 'com.google.android.gms:play-services-ads:+'")
		env.android_add_dependency("compile files('../../../modules/chartboost/android/lib/chartboost.jar')")
		env.android_add_to_manifest("android/AndroidManifestChunk.xml")
		env.android_add_java_dir("android")
		#env.disable_module()

